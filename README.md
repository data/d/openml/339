# OpenML dataset: pasture

https://www.openml.org/d/339

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Dave Barker  
**Source**: [original](http://www.cs.waikato.ac.nz/ml/weka/datasets.html) -   
**Please cite**:   

Pasture Production

Data source:   Dave Barker
AgResearch Grasslands, Palmerston North, New Zealand

The objective was to predict pasture production from a variety of biophysical factors. Vegetation and soil variables from areas of grazed North Island hill country with different management (fertilizer application/stocking rate) histories (1973-1994) were measured and subdivided into 36 paddocks. Nineteen vegetation (including herbage production); soil chemical, physical and biological; and soil water variables were selected as potentially useful biophysical indicators.

Attribute Information:
1.  fertiliser - fertiliser used - enumerated
2.  slope - slope of the paddock - integer
3.  aspect-dev-NW - the deviation from the north-west - integer
4.  OlsenP - integer
5.  MinN - integer
6.  TS - integer
7.  Ca-Mg - calcium magnesium ration - real
8.  LOM - soil lom (g/100g) - real
9.  NFIX-mean - a mean calculation - real
10. Eworms-main-3 - main 3 spp earth worms per g/m2 - real
11. Eworms-No-species - number of spp - integer
12. KUnSat - mm/hr - real
13. OM - real
14. Air-Perm - real
15. Porosity - real
16. HFRG-pct-mean - mean percent - real
17. legume-yield - kgDM/ha - real
18. OSPP-pct-mean - mean percent - real
19. Jan-Mar-mean-TDR - real
20. Annual-Mean-Runoff - in mm - real
21. root-surface-area - m2/m3 - real
22. Leaf-P - ppm - real
23. pasture-prod-class - pasture production categorisation - enumerated

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/339) of an [OpenML dataset](https://www.openml.org/d/339). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/339/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/339/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/339/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

